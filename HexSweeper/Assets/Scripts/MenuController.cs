﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    [SerializeField] Canvas Home, Size, Difficulty, Levels;

    private void Start()
    {
        SetHome();
    }

    public void SetHome()
    {
        Home.gameObject.SetActive(true);
        Size.gameObject.SetActive(false);
        Difficulty.gameObject.SetActive(false);
        Levels.gameObject.SetActive(false);
    }

    public void SetSize()
    {
        Home.gameObject.SetActive(false);
        Size.gameObject.SetActive(true);
        Difficulty.gameObject.SetActive(false);
        Levels.gameObject.SetActive(false);
    }

    public void SetDifficulty()
    {
        Home.gameObject.SetActive(false);
        Size.gameObject.SetActive(false);
        Difficulty.gameObject.SetActive(true);
        Levels.gameObject.SetActive(false);
    }
    public void SetLevels()
    {
        Home.gameObject.SetActive(false);
        Size.gameObject.SetActive(false);
        Difficulty.gameObject.SetActive(false);
        Levels.gameObject.SetActive(true);
    }

}
