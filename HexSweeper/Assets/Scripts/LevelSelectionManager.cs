﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectionManager : MonoBehaviour
{
    [SerializeField] GameObject button;
    int size, difficulty;

    private void Start()
    {
        Level[] levels = Resources.LoadAll<Level>("Levels/" + size + "/" + difficulty);
        int levelCount = levels.Length;

        GameObject go;
        for (int i = 0; i < levelCount; i++)
        {
            go = Instantiate(button, this.transform);
            go.GetComponent<ButtonController>().level = i + 1;
        }
    }

    public void SetSize(int _size)
    {
        size = _size;
        PlayerPrefs.SetInt("size", size);
    }
    public void SetDifficulty(int _difficulty)
    {
        difficulty = _difficulty;
        PlayerPrefs.SetInt("difficulty", difficulty);
    }
}
