﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hex : MonoBehaviour
{
    // Set on Inspector
    [SerializeField] TextMesh textMesh;
    [SerializeField] Material[] materials;
    public Vector3 pos;
    public Hex[] neighbors;
    public int value;
    public bool selected, empty;

    public void UpdateTextMesh()
    {
        if (value == 0) 
        {
            textMesh.gameObject.SetActive(false);
        }
        else
        {
            textMesh.gameObject.SetActive(true);
            textMesh.text = value.ToString();
        }
    }

    public void HideTextMesh()
    {
        textMesh.gameObject.SetActive(false);
    }

    public void UpdateValue()
    {
        if(value != 10)
        {
            int auxCounter = 0;
            foreach (Hex neighbor in neighbors)
            {
                if(neighbor != null && neighbor.value == 10) auxCounter++;
            }
            value = auxCounter;
        }
    }

    public void Play()
    {
        if(value == 10)
        {
            HexCore.instance.gameState = HexCore.GameState.GameOver;
            GameOver();
        }
        else
        {
            if(value == 0)
            {
                if(!empty)
                {
                    empty = true;
                    transform.GetComponentInChildren<MeshRenderer>().material = materials[1];
                    foreach (Hex neighbor in neighbors)
                    {
                        if(neighbor != null && neighbor.value != 10)
                        {
                            neighbor.Play();
                        }
                    }
                }
            }
            else
            {
                UpdateTextMesh();
            }
        }
    }

    public void Select()
    {
        if(!empty && textMesh.gameObject.activeSelf == false )
        {
            if(selected)
            {
                selected = false;
                transform.GetComponentInChildren<MeshRenderer>().material = materials[0];
            }
            else
            {
                selected = true;
                transform.GetComponentInChildren<MeshRenderer>().material = materials[2];
            }
        }
    }

    public void GameOver()
    {
        transform.GetComponentInChildren<MeshRenderer>().material = materials[3];
    }
}
