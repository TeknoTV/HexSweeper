﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HexCore : MonoBehaviour
{
    public enum GameState
    {
        Play,
        Win,
        GameOver
    }

    #region Singleton
    public static HexCore instance;
    public GameState gameState;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);
    }
    #endregion

    [SerializeField] Level[] levels;
    public Vector2[,] grid;
    public Transform hexPrefab;
    List<List<Hex>> hexGrid;

    int currentLevel = 0;
    int width, height, mineCount;
    public float gap = 0.1f;
    float hexWidth = 1.732f;
    float hexHeight = 2f;

    int levelSize = 1, levelDifficulty;

    int selectedCounter;
    Vector2 startHex, endHex;

    // ### Camera
    public Camera camera;
    public List<Transform> targets;

    bool flag = false;

    Vector3 offset;

    #region Start
    void Start()
    {
        levelSize = PlayerPrefs.GetInt("size");
        levelDifficulty = PlayerPrefs.GetInt("levelDifficulty");
        offset = levelSize == 3 ? new Vector3(0, 100, -4) : new Vector3(0, 100, -2.5f);
        gameState =  GameState.Play;
        levels = Resources.LoadAll<Level>("Levels/" + levelSize + "/" + levelDifficulty);
        width = levels[currentLevel].width;
        height = levels[currentLevel].height;
        mineCount = levels[currentLevel].mineCount;
        grid = new Vector2[width, height];
        hexGrid = new List<List<Hex>>();

        // Generate Grid
        AddGap();
        CreateHexagonsGrid();
        DrawHexagons();
        CalculateNeighbors();
        UpdateCamera();
    }
    #endregion

    #region Update
    private void Update()
    {
        if(gameState == GameState.Play)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 300))
                {
                    if(!flag)
                    {
                        hit.transform.GetComponentInParent<Hex>().Play();
                    }
                    else
                    {
                        hit.transform.GetComponentInParent<Hex>().Select();
                        Debug.Log(IsWin());
                        if(IsWin())
                        {
                            gameState = GameState.Win;
                        }
                    }
                }
            }
        }
        else if(gameState == GameState.GameOver)
        {
            GameOver();
        }
        else if(gameState == GameState.Win)
        {
            Win();
        }
    }
    #endregion

    #region Generate Hex Grid
    void CreateHexagonsGrid()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                grid[x, y] = new Vector2(x, y);
            }
        }
    }

    void DrawHexagons()
    {
        List<Hex> auxList;
        for (int h = 0; h < height; h++)
        {
            auxList = new List<Hex>();
            for (int w = 0; w < width; w++)
            {
                if (grid[w, h] != null)
                {
                    Transform hex = Instantiate(hexPrefab, WorldCoords(grid[w, h]), Quaternion.identity, this.transform);
                    hex.name = "(" + grid[w, h].x + "," + grid[w, h].y + ")";
                    hex.GetComponent<Hex>().value = levels[currentLevel].values[w + h * width];
                    hex.GetComponent<Hex>().pos = OffsetToCubeCoords(grid[w, h]);
                    hex.GetComponent<Hex>().HideTextMesh();
                    auxList.Add(hex.GetComponent<Hex>());
                    targets.Add(hex);
                }
            }
            hexGrid.Add(auxList);
        }
    }

    Vector3 WorldCoords(Vector2 gridPos)
    {
        float offset = 0;
        if (gridPos.y % 2 != 0)
        {
            offset = hexWidth / 2;
        }

        float x = transform.position.x * hexWidth + gridPos.x * hexWidth + offset;
        float z = transform.position.z * hexHeight + gridPos.y * hexHeight * 0.75f;

        return new Vector3(x, 0, z);
    }

    Vector3 OffsetToCubeCoords(Vector2 coords)
    {
        int auxX = (int)coords.x, auxY = (int)coords.y;
        var x = auxX - (auxY - (auxY & 1)) / 2;
        var z = auxY;
        var y = -x - z;
        return new Vector3(x, y, z);
    }

    void CalculateNeighbors()
    {
        for (int h = 0; h < hexGrid.Count; h++)
        {
            for (int w = 0; w < hexGrid[h].Count; w++)
            {
                hexGrid[h][w].neighbors = GetHexNeighbors(CubeToOffset(hexGrid[h][w].pos));
                hexGrid[h][w].UpdateValue();
            }
        }
    }

    Hex[] GetHexNeighbors(Vector2 coords)
    {
        Vector3 aux = OffsetToCubeCoords(coords);
        Vector3[] cube_directions = { new Vector3(0, -1, 1), new Vector3(1, -1, 0), new Vector3(1, 0, -1), new Vector3(0, 1, -1), new Vector3(-1, 1, 0), new Vector3(-1, 0, 1) };
        Hex[] hexAux = new Hex[6];
        for (int i = 0; i < 6; i++)
        {
            cube_directions[i] += aux;
            for (int h = 0; h < hexGrid.Count; h++)
            {
                for (int w = 0; w < hexGrid[h].Count; w++)
                {
                    if(hexGrid[h][w].pos == cube_directions[i])
                    {
                        hexAux[i] = hexGrid[h][w].GetComponent<Hex>();
                    }
                }
                }
        }
        return hexAux;
    }

    Vector2 CubeToOffset(Vector3 cube)
    {
        int col = (int)cube.x + ((int)cube.z - ((int)cube.z & 1)) / 2;
        int row = (int)cube.z;
        return new Vector2(col, row);
    }

    void AddGap()
    {
        hexWidth += hexWidth * gap;
        hexHeight += hexHeight * gap;
    }
    #endregion

    #region Game Control
        public void ToggleFlag()
        {
            flag = !flag;
        }
        
        bool IsWin()
        {
            int auxCounter = 0;
            int selectedCounter = 0;
            for (int h = 0; h < hexGrid.Count; h++)
            {
                for (int w = 0; w < hexGrid[h].Count; w++)
                {
                    if(hexGrid[h][w].value == 10)
                    {
                        auxCounter++;
                        if(hexGrid[h][w].selected)
                        {
                            selectedCounter++;
                        }
                    }
                }
            }

            return auxCounter == selectedCounter;
        }

        void Win()
        {
            SceneManager.LoadScene("MainMenu");
        }
        void GameOver()
        {
            for (int h = 0; h < hexGrid.Count; h++)
            {
                for (int w = 0; w < hexGrid[h].Count; w++)
                {
                    if(hexGrid[h][w].value == 10)
                    {
                        hexGrid[h][w].GameOver();
                    }
                }
            }
            SceneManager.LoadScene("Game");
        }
    #endregion

    #region Camera
    void UpdateCamera()
    {
        Vector3 centerPoint = GetCenterPoint();

        Vector3 newPosition = centerPoint + offset;

        camera.transform.position = newPosition;

        camera.fieldOfView = GetGreatestDistance();
    }
    Vector3 GetCenterPoint()
    {
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        return bounds.center + new Vector3(0, 0, height / 6);
    }

    float GetGreatestDistance()
    {
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        return bounds.size.x * 1.1f + hexWidth;
    }
    #endregion

}
