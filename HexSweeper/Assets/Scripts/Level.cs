﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Level")]
public class Level : ScriptableObject
{
    public int width, height, mineCount;
    public int[] values;
    
    public void GenerateLevel()
    {
        values = new int[width * height];

        for (int i = 0; i < mineCount; i++)
        {
            int j = Random.Range(0, width);
            int k = Random.Range(0, height);
            if(values[j + k * width] != 10)
            {
                values[j + k * width] = 10;
            }
            else
            {
                i--;
            }
        }
    }
}
