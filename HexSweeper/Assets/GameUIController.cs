﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{
    public Image button;
    public bool selected = false;

    private void Start()
    {
        button.color = new Color(111/255, 111/255, 111/255, 1);
    }

    public void ToggleSelected()
    {
        selected = !selected;
        button.color = selected ? new Color(1, 1, 1, 1) : new Color(111/255, 111/255, 111/255, 1);
    }
}
