﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Level))]
public class GenerateLevelOnInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Level level = (Level)target;

        if (GUILayout.Button("Generate Level"))
        {
            level.GenerateLevel();
        }
    }
}